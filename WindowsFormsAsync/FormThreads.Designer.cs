﻿namespace WindowsFormsAsync
{
    partial class FormThreads
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonThread = new System.Windows.Forms.Button();
            this.buttonInterrupt = new System.Windows.Forms.Button();
            this.richTextBoxInfo = new System.Windows.Forms.RichTextBox();
            this.buttonResume = new System.Windows.Forms.Button();
            this.buttonSuspend = new System.Windows.Forms.Button();
            this.buttonThreadStart2 = new System.Windows.Forms.Button();
            this.buttonStartObject = new System.Windows.Forms.Button();
            this.buttonThreadStartObjectMethodParameter = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonThread
            // 
            this.buttonThread.Location = new System.Drawing.Point(91, 58);
            this.buttonThread.Name = "buttonThread";
            this.buttonThread.Size = new System.Drawing.Size(101, 34);
            this.buttonThread.TabIndex = 0;
            this.buttonThread.Text = "Thread Start";
            this.buttonThread.UseVisualStyleBackColor = true;
            this.buttonThread.Click += new System.EventHandler(this.ButtonThread_Click);
            // 
            // buttonInterrupt
            // 
            this.buttonInterrupt.Location = new System.Drawing.Point(91, 163);
            this.buttonInterrupt.Name = "buttonInterrupt";
            this.buttonInterrupt.Size = new System.Drawing.Size(97, 27);
            this.buttonInterrupt.TabIndex = 1;
            this.buttonInterrupt.Text = "Interrupt";
            this.buttonInterrupt.UseVisualStyleBackColor = true;
            this.buttonInterrupt.Click += new System.EventHandler(this.ButtonInterrupt_Click);
            // 
            // richTextBoxInfo
            // 
            this.richTextBoxInfo.Location = new System.Drawing.Point(542, 63);
            this.richTextBoxInfo.Name = "richTextBoxInfo";
            this.richTextBoxInfo.Size = new System.Drawing.Size(150, 102);
            this.richTextBoxInfo.TabIndex = 2;
            this.richTextBoxInfo.Text = "";
            // 
            // buttonResume
            // 
            this.buttonResume.Location = new System.Drawing.Point(91, 130);
            this.buttonResume.Name = "buttonResume";
            this.buttonResume.Size = new System.Drawing.Size(97, 27);
            this.buttonResume.TabIndex = 3;
            this.buttonResume.Text = "Resume";
            this.buttonResume.UseVisualStyleBackColor = true;
            this.buttonResume.Click += new System.EventHandler(this.ButtonResume_Click);
            // 
            // buttonSuspend
            // 
            this.buttonSuspend.Location = new System.Drawing.Point(91, 97);
            this.buttonSuspend.Name = "buttonSuspend";
            this.buttonSuspend.Size = new System.Drawing.Size(97, 27);
            this.buttonSuspend.TabIndex = 4;
            this.buttonSuspend.Text = "Suspend";
            this.buttonSuspend.UseVisualStyleBackColor = true;
            this.buttonSuspend.Click += new System.EventHandler(this.ButtonSuspend_Click);
            // 
            // buttonThreadStart2
            // 
            this.buttonThreadStart2.Location = new System.Drawing.Point(698, 63);
            this.buttonThreadStart2.Name = "buttonThreadStart2";
            this.buttonThreadStart2.Size = new System.Drawing.Size(101, 33);
            this.buttonThreadStart2.TabIndex = 5;
            this.buttonThreadStart2.Text = "Task Start";
            this.buttonThreadStart2.UseVisualStyleBackColor = true;
            this.buttonThreadStart2.Click += new System.EventHandler(this.ButtonThreadStart2_Click);
            // 
            // buttonStartObject
            // 
            this.buttonStartObject.Location = new System.Drawing.Point(209, 58);
            this.buttonStartObject.Name = "buttonStartObject";
            this.buttonStartObject.Size = new System.Drawing.Size(118, 34);
            this.buttonStartObject.TabIndex = 6;
            this.buttonStartObject.Text = "Thread Start Object";
            this.buttonStartObject.UseVisualStyleBackColor = true;
            this.buttonStartObject.Click += new System.EventHandler(this.ButtonStartObject_Click);
            // 
            // buttonThreadStartObjectMethodParameter
            // 
            this.buttonThreadStartObjectMethodParameter.Location = new System.Drawing.Point(209, 98);
            this.buttonThreadStartObjectMethodParameter.Name = "buttonThreadStartObjectMethodParameter";
            this.buttonThreadStartObjectMethodParameter.Size = new System.Drawing.Size(211, 34);
            this.buttonThreadStartObjectMethodParameter.TabIndex = 7;
            this.buttonThreadStartObjectMethodParameter.Text = "parameterizedThreadStart - not type safe";
            this.buttonThreadStartObjectMethodParameter.UseVisualStyleBackColor = true;
            this.buttonThreadStartObjectMethodParameter.Click += new System.EventHandler(this.ButtonThreadStartObjectMethodParameter_Click);
            // 
            // FormThreads
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonThreadStartObjectMethodParameter);
            this.Controls.Add(this.buttonStartObject);
            this.Controls.Add(this.buttonThreadStart2);
            this.Controls.Add(this.buttonSuspend);
            this.Controls.Add(this.buttonResume);
            this.Controls.Add(this.richTextBoxInfo);
            this.Controls.Add(this.buttonInterrupt);
            this.Controls.Add(this.buttonThread);
            this.Name = "FormThreads";
            this.Text = "FormThreads";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonThread;
        private System.Windows.Forms.Button buttonInterrupt;
        private System.Windows.Forms.RichTextBox richTextBoxInfo;
        private System.Windows.Forms.Button buttonResume;
        private System.Windows.Forms.Button buttonSuspend;
        private System.Windows.Forms.Button buttonThreadStart2;
        private System.Windows.Forms.Button buttonStartObject;
        private System.Windows.Forms.Button buttonThreadStartObjectMethodParameter;
    }
}
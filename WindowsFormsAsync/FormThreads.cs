﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsAsync
{
    public partial class FormThreads : Form
    {
        private Thread thread;
        Task task;
        private Object _lock = new object();
        private Semaphore semaphore = new Semaphore(1, 1);

        public delegate void sumOfNumber(int finalNumber);

        public FormThreads()
        {
            InitializeComponent();
        }

        private void ButtonThread_Click(object sender, EventArgs e)
        {
            try
            {
                thread = new Thread(Process);
                thread.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
            }
        }

        private void ButtonThreadStart2_Click(object sender, EventArgs e)
        {
            try
            {
                task = new Task(Process2);
                task.Start();
            }
            catch (AggregateException ex)
            {
                MessageBox.Show("caught in upper Thread" + ex.StackTrace);
            }
        }

        private void ButtonStartObject_Click(object sender, EventArgs e)
        {
            try
            {
                Local local = new Local(50, this);
                Thread thread = new Thread(new ThreadStart(local.Run));
                thread.Start();
            }
            catch (AggregateException ex)
            {
                MessageBox.Show("caught in upper Thread" + ex.StackTrace);
            }
        }

        private void ButtonThreadStartObjectMethodParameter_Click(object sender, EventArgs e)
        {
            try
            {
                Local local = new Local(50, this);
                //not necessary explicit
                //ParameterizedThreadStart parameterizedThreadStart = new ParameterizedThreadStart(local.Run1);
                //Thread thread = new Thread(parameterizedThreadStart);
                //implicit ParameterizedThreadStart
                Thread thread = new Thread(local.Run1);
                int step = 2;
                //is not type safe :(   is an object sent
                //better to initialize in the construtor
                //and maintain type safe - like we have done before
                thread.Start(step);
            }
            catch (AggregateException ex)
            {
                MessageBox.Show("caught in upper Thread" + ex.StackTrace);
            }
        }

        private void Process()
        {
            try
            {
                Action actionClean = () => richTextBoxInfo.Clear();
                this.BeginInvoke(actionClean);
                int i = 0;
                Action actionUpdate = () => richTextBoxInfo.Text += "\n" + i;
                for (i = 0; i < 1000; i++)
                {
                    semaphore.WaitOne();
                    this.BeginInvoke(actionUpdate);
                    Thread.Sleep(1000);
                    semaphore.Release();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception caught in the Thread " + ex.StackTrace);
            }
        }


        private void Process2()
        {
            try
            {
                Action actionClean = () => richTextBoxInfo.Clear();
                this.BeginInvoke(actionClean);
                int i = 0;
                Action actionUpdate = () => richTextBoxInfo.Text += "\n" + i;
                for (i = 0; i < 1000; i++)
                {
                    semaphore.WaitOne();
                    this.BeginInvoke(actionUpdate);
                    Thread.Sleep(1000);
                    semaphore.Release();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void ButtonInterrupt_Click(object sender, EventArgs e)
            => thread.Interrupt();

        private void ButtonSuspend_Click(object sender, EventArgs e)
            => semaphore.WaitOne();

        private void ButtonResume_Click(object sender, EventArgs e)
            => semaphore.Release();


        class Local
        {
            FormThreads formThreads;
            int value;
            public Local(int value, FormThreads formThreads)
            {
                this.value = value;
                this.formThreads = formThreads;
            }
            public void Run()
            {
                Action actionClean = () => formThreads.richTextBoxInfo.Clear();
                formThreads.BeginInvoke(actionClean);
                while (value > 0)
                {
                    try
                    {
                        Action actionUpdate = () => formThreads.richTextBoxInfo.Text += "\n" + value;
                        formThreads.BeginInvoke(actionUpdate);
                        Thread.Sleep(1000);
                    }
                    catch (System.InvalidOperationException)
                    {

                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show("stack trace " + ex.StackTrace);
                    }

                    value--;
                }
            }
            public void Run1(object _step)
            {
                if (int.TryParse(_step.ToString(), out int step))
                {
                    Action actionClean = () => formThreads.richTextBoxInfo.Clear();
                    formThreads.BeginInvoke(actionClean);
                    while (value > 0)
                    {
                        try
                        {
                            Action actionUpdate = () => formThreads.richTextBoxInfo.Text += "\n" + value;
                            formThreads.BeginInvoke(actionUpdate);
                            Thread.Sleep(1000);
                        }
                        catch (System.InvalidOperationException)
                        {

                        }
                        catch (Exception ex)
                        {

                            MessageBox.Show("stack trace " + ex.StackTrace);
                        }

                        value = value - step;
                    }
                }
            }
        }


    }
}

﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsAsync
{
    public partial class FormUI_Update : Form
    {
        public FormUI_Update()
        {
            InitializeComponent();
        }

        private async void Button1_Click(object sender, EventArgs e)
        {
            Task<int> task = new Task<int>(Process);
            task.Start();
            label1.Text = "Processing...";
            int value = await task;
            label1.Text = "valor=" + value;
        }

        private int Process()
        {
            Thread.Sleep(5000);
            return 10;
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            int count = 0;

            Thread thread = new Thread(
                () =>
                {
                    count = Process();
                    //vamos pedir à Thread da UI para executar determinado código
                    Action action = () => label2.Text = "valor=" + count;
                    this.BeginInvoke(action);
                }
                );
            thread.Start();
            label2.Text = "Processing...";



        }

        private void Button2_Blocking_Click(object sender, EventArgs e)
        {
            int count = 0;
            label2.Text = "Processing...";
            Thread thread = new Thread(
                () => count = Process()
                );
            thread.Start();
            thread.Join();


            label2.Text = "valor=" + count;
        }
    }
}
